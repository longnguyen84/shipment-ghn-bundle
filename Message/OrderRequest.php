<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 11:31 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Message;


use Nilead\ShipmentCommonBundle\Message\OrderRequestTrait;
use Nilead\ShipmentCommonComponent\Message\OrderRequestInterface;
use Nilead\ShipmentGHNBundle\DistrictCodeMapper;

class OrderRequest extends RequestAbstract implements OrderRequestInterface
{
    use OrderRequestTrait;

    public function setWeight($weight)
    {
        return $this->setParameter('Weight', $weight);
    }

    public function getWeight()
    {
        return $this->getParameter('Weight');
    }

    public function setFromDistrictCode($fromDistrictCode)
    {
        return $this->setParameter('FromDistrictCode', $fromDistrictCode);
    }

    public function getFromDistrictCode()
    {
        return $this->getParameter('FromDistrictCode');
    }

    public function setToDistrictCode($toDistrictCode)
    {
        return $this->setParameter('ToDistrictCode', $toDistrictCode);
    }

    public function getToDistrictCode()
    {
        return $this->getParameter('ToDistrictCode');
    }

    public function setShippingServiceID($shippingServiceID)
    {
        return $this->setParameter('ShippingServiceID', $shippingServiceID);
    }

    public function getShippingServiceID()
    {
        return $this->getParameter('ShippingServiceID');
    }

    public function getDefaultParameters()
    {
        return array_merge(parent::getDefaultParameters(),
            [
                'SessionToken' => '',
                'Weight' => '',
                'FromDistrictCode' => '',
                'ToDistrictCode' => '',
                'ShippingServiceID' => ''
            ]);
    }

    public function getData()
    {
        $data = array_merge($this->getBaseData(),
            [
                "RecipientCode" => $this->getRecipientId(),
                "RecipientName" => $this->getPackage()->getShipToAddress()->getLastName() . ' ' . $this->getPackage()->getShipToAddress()->getFirstName(),
                "RecipientPhone" => $this->getPackage()->getShipToAddress()->getPhone(),
                "DeliveryAddress" => $this->getPackage()->getShipToAddress()->getAddress1() . ' ' . $this->getPackage()->getShipToAddress()->getAddress2(),
                "DeliveryDistrictCode" => DistrictCodeMapper::fromGeoCode($this->getPackage()->getShipToAddress()),
                "CODAmount" => $this->getCOD(),
                "Note" => $this->getNote(),
                "VendorCode" => $this->getStoreId(),
                "ShopTransactionCode" => $this->getOrderId(),
                "SenderName" => $this->getPackage()->getShipToAddress()->getLastName() . ' ' . $this->getPackage()->getShipToAddress()->getFirstName(),
                "SenderPhone" => $this->getPackage()->getShipToAddress()->getPhone(),
                "SenderAddress" => $this->getPackage()->getShipFromAddress()->getAddress1() . ' ' . $this->getPackage()->getShipFromAddress()->getAddress2(),
                "PickAddress" => $this->getPackage()->getShipFromAddress()->getAddress1() . ' ' . $this->getPackage()->getShipFromAddress()->getAddress2(),
                "PickDistrictCode" => DistrictCodeMapper::fromGeoCode($this->getPackage()->getShipFromAddress()),
                "ServiceToUse" => $this->getPackage()->getShippingMethod()->getMethodCode(),
                'SessionToken' => $this->getSessionToken()
            ]);

        return $data;
    }

    public function sendData($data)
    {
        $httpResponse = $this->httpClient->post($this->getEndpoint() . '/CreateShippingOrder', array('Content-Type' => 'application/json'), json_encode($data))->send();file_put_contents(__DIR__ . '/test2.txt', $httpResponse->getBody());
        return $this->response = new OrderResponse($this, $httpResponse->json());
    }

}
