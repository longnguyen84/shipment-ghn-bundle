<?php
/**
 * Created by Rubikin Team.
 * Date: 6/3/14
 * Time: 3:25 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Message;


use Nilead\ShipmentCommonComponent\Message\ResponseInterface;
use Nilead\ShipmentCommonBundle\Message\ResponseAbstract as BaseResponseAbstract;

abstract class ResponseAbstract extends BaseResponseAbstract implements ResponseInterface
{
    public function getMessage()
    {
        return NULL == $this->data['ResponseException'] ? NULL : $this->data['ResponseException']['ErrorMessage'];
    }
}
